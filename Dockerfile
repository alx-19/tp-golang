# Utiliser une image de base officielle de Go.
FROM golang:1.18 as builder

# Définir le répertoire de travail dans le conteneur.
WORKDIR /app

# Copier les fichiers du projet dans le conteneur.
COPY . .

# Définir le répertoire de travail pour la compilation à l'emplacement de main.go.
WORKDIR /app/cmd

# Compiler l'application.
RUN go build -v -o main .

# Définir le répertoire de travail pour l'exécution à la racine de l'app.
WORKDIR /app

# Déplacer l'exécutable au bon endroit
RUN mv cmd/main .

# Exécuter l'application.
CMD ["./main"]
