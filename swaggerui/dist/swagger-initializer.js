window.onload = function() {
  // Initialisation de Swagger UI avec l'URL de ta spécification Swagger
  const ui = SwaggerUIBundle({
    url: "http://localhost:9000/swagger/swagger.yaml",
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
  });

  window.ui = ui;
};
