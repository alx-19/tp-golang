## Go 2FA (Two-Factor Authentication)

### What is 2FA?
2FA stands for Two Factor Authentication. It is a method of confirming users' claimed identities by using a combination
of two different factors: 1) something they know, 2) something they have, or 3) something they are.

### Why 2FA?
2FA provides an extra layer of security and makes it harder for attackers to gain access to a person's devices and
online accounts, because knowing the victim's password alone is not enough to pass the authentication check.

### How does 2FA work?
When a user tries to log in to a website or service, they will be asked to provide a second piece of information in
addition to their password. This second piece of information can be a code sent to their phone, a fingerprint, or a
security key.

### A basic 2FA Go implementation

#### Auth Server
- Generate a 6 digit random code and store it with the user's ID
- Create a new expiration time for the code (30 seconds)
- Send the code to the user

#### User client
- Ask a code from the user
- Receive the code from auth server
- Send the code to the Resource Server
- Receive the response from the Resource Server
    - If the response is `200 OK`, grant access to the user
    - If the response is `401 Unauthorized`, deny access to the user

#### Resource Server
- Check if the code is correct by sending it to the Auth Server
    - If the code is correct, grant access to the user
    - If the code is incorrect, deny access to the user
    - If the code has expired, deny access to the user
    - If the code has been used before, deny access to the user
    - If the code is correct, delete it from the Auth Server

### Improvements

- Use a more secure way to generate the code
- Use Protocol Buffers/gRPC to communicate between the servers and the client
- Use a Swagger API to document the API
    - Generate the client and server code from the Swagger API
- Use a library to handle the 2FA
    - https://github.com/pquerna/otp
- Handle more than 2.880.000.000 codes by day (60 seconds * 2000000 tokens * 24 hours)
- Benchmark the code to see how efficient it is
- Add TLS (local certificates) or Let's Encrypt to secure the communication between the servers and the client

## adresse pour rendre le projet christophe.lecroq@gmail.com