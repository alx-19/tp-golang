package auth

import (
	"errors"
	"fmt"
	"github.com/pquerna/otp/totp"
	"github.com/skip2/go-qrcode"
)

// User représente un utilisateur du système.
type User struct {
	ID        string
	Username  string
	Password  string // Dans un cas réel hash du mot de passe. si j'ai le temps je le ferai, sinon je le laisserai tel quel.
	Secret2FA string // Clé secrète pour 2FA.
	TotpURL   string // URL TOTP pour générer le QR Code.
}

// UserManager gère les utilisateurs enregistrés en mémoire.
type UserManager struct {
	users map[string]User
}

// NewUserManager crée et retourne une nouvelle instance de UserManager.
func NewUserManager() *UserManager {
	return &UserManager{
		users: make(map[string]User),
	}
}

// CreateUser crée un nouvel utilisateur et le stocke.
func (um *UserManager) CreateUser(id, username, password string) error {
	if _, exists := um.users[id]; exists {
		return errors.New("user already exists")
	}

	// Ajout de la clé secrète et de l'URL TOTP lors de la création de l'utilisateur
	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      "MonApplication",
		AccountName: username,
	})
	if err != nil {
		return err
	}

	um.users[id] = User{
		ID:        id,
		Username:  username,
		Password:  password, // *** hashé.
		Secret2FA: key.Secret(),
		TotpURL:   key.URL(),
	}

	return nil
}

// Dans userManager.go
func (um *UserManager) GetUser(userID string) (User, bool) {
	user, exists := um.users[userID]
	return user, exists
}

// Authenticate vérifie les identifiants de l'utilisateur.
func (um *UserManager) Authenticate(username, password string) (string, bool) {
	for id, user := range um.users {
		if user.Username == username && user.Password == password {
			// L'authentification réussie retourne l'ID de l'utilisateur.
			return id, true
		}
	}
	return "", false
}

// Exists vérifie si un utilisateur existe en fonction de son ID.
func (um *UserManager) Exists(userID string) bool {
	_, exists := um.users[userID]
	return exists
}

// Setup2FA permet de générer un QR Code pour l'utilisateur basé sur sa clé secrète TOTP.
// Cette méthode peut être appelée séparément si la configuration 2FA doit être activée après l'inscription.
func (um *UserManager) Setup2FA(userID string) (string, error) {
	user, exists := um.users[userID]
	if !exists {
		return "", fmt.Errorf("user not found")
	}

	// Générer le nom du fichier QR Code.
	qrFileName := fmt.Sprintf("qr_%s.png", userID) // Nom du fichier seul

	// Définir le chemin complet pour enregistrer le QR Code
	qrFilePath := fmt.Sprintf("./internal/qrcode/%s", qrFileName) // Chemin complet du fichier

	// Générer et sauvegarder le QR Code en utilisant le chemin complet
	if err := qrcode.WriteFile(user.TotpURL, qrcode.Medium, 256, qrFilePath); err != nil {
		return "", err
	}

	// Retourner le nom du fichier (sans le chemin) pour l'utiliser dans l'URL
	return qrFileName, nil
}
