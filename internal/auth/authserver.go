package auth

import (
	"context"
	"github.com/go-redis/redis/v8"
	"log"
	"math/rand"
	"strconv"
	"time"
)

type AuthServer struct {
	redisClient *redis.Client
	userManager *UserManager
}

func NewAuthServer(redisClient *redis.Client, userManager *UserManager) *AuthServer {
	return &AuthServer{
		redisClient: redisClient,
		userManager: userManager,
	}
}

func (a *AuthServer) GenerateCode(userID string) string {
	code := strconv.Itoa(rand.Intn(999999-100000) + 100000)
	err := a.redisClient.Set(context.Background(), code, userID, 30*time.Second).Err()
	if err != nil {
		log.Printf("Failed to set code in Redis: %v", err)
	} else {
		log.Printf("Code %s set for user %s", code, userID)
	}
	return code
}

func (a *AuthServer) ValidateCode(code string) bool {
	_, err := a.redisClient.Get(context.Background(), code).Result()
	if err == redis.Nil {
		return false
	} else if err != nil {
		return false
	}

	a.redisClient.Del(context.Background(), code)
	return true
}

func (a *AuthServer) GetCodeExpiration(code string) time.Time {
	res, err := a.redisClient.TTL(context.Background(), code).Result()
	if err != nil || res == -2 {
		return time.Time{} // Return zero time if no expiration or an error occurs
	}
	return time.Now().Add(res)
}
