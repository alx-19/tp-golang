package auth

import (
	"testing"
)

func BenchmarkGenerateCode(b *testing.B) {
	userManager := NewUserManager()                         // Crée une instance de UserManager
	userManager.CreateUser("123456", "User", "password123") // Prépare un utilisateur de test

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = userManager.Setup2FA("testuser") // Appelle la méthode de génération de code
	}
}
