package client

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

// RequestCode envoie une requête au serveur d'authentification pour obtenir un code de vérification.
func RequestCode(authServerURL string, userID string) {
	// Construit l'URL de requête en incorporant l'ID de l'utilisateur.
	url := fmt.Sprintf("%s/generate?userID=%s", authServerURL, userID)

	// Effectue la requête HTTP GET à l'URL spécifiée.
	resp, err := http.Get(url)
	if err != nil {
		// Si une erreur survient lors de la requête, affiche l'erreur et arrête la fonction.
		fmt.Printf("Erreur lors de la demande de code : %v\n", err)
		return
	}

	// S'assure que le corps de la réponse est fermé à la fin de la fonction.
	// Cette étape est importante pour libérer les ressources réseau.
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			// Si une erreur survient lors de la fermeture, elle est affichée.
			fmt.Printf("Erreur lors de la fermeture du corps de la réponse : %v\n", err)
		}
	}(resp.Body)

	// Lit le corps de la réponse HTTP.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// Si une erreur survient lors de la lecture, affiche l'erreur et arrête la fonction.
		fmt.Printf("Erreur lors de la lecture de la réponse : %v\n", err)
		return
	}

	// Affiche le corps de la réponse, qui devrait contenir le code de vérification.
	fmt.Printf("Code reçu du serveur d'authentification : %s\n", string(body))
}
