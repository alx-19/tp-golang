package server

import (
	"fmt"
	"net/http"
	"tp-golang/internal/auth"
)

// ResourceServer représente le serveur qui contient les ressources protégées.
type ResourceServer struct {
	authServer *auth.AuthServer // authServer est une référence au serveur d'authentification pour valider les codes.
}

// NewResourceServer crée et retourne une nouvelle instance de ResourceServer.
func NewResourceServer(authServer *auth.AuthServer) *ResourceServer {
	return &ResourceServer{
		authServer: authServer,
	}
}

// ProtectedHandler est le gestionnaire HTTP qui protège l'accès aux ressources.
// vérifie si le code fourni par l'utilisateur est valide.
func (rs *ResourceServer) ProtectedHandler(w http.ResponseWriter, r *http.Request) {
	// Extraire le code de la requête HTTP.
	code := r.URL.Query().Get("code")

	// Valider le code avec le serveur d'authentification.
	if rs.authServer.ValidateCode(code) {
		// Si le code est valide, accorder l'accès et envoyer "Access Granted" comme réponse.
		_, err := fmt.Fprintln(w, "Access Granted")
		// Gestion de l'erreur potentielle de fmt.Fprintln. Si une erreur survient,
		// la fonction retourne immédiatement pour éviter d'exécuter le reste du code.
		if err != nil {
			// Ici, tu pourrais également choisir de logger l'erreur ou d'envoyer une réponse HTTP spécifique.
			return
		}
		return
	}
	// Si le code n'est pas valide, refuser l'accès avec le statut HTTP 401 Unauthorized.
	http.Error(w, "Access Denied", http.StatusUnauthorized)
}

func (rs *ResourceServer) ValidateCode(code string) bool {
	return rs.authServer.ValidateCode(code)
}
