package handlers

import (
	"fmt"
	"net/http"
	"tp-golang/internal/auth"
)

// SignupHandler gère la page d'inscription des utilisateurs.
// @Summary Signup a new user
// @Description Signup a new user by providing ID, username, and password. User will then need to setup 2FA with the provided QR code.
// @Tags users
// @Accept json
// @Produce json
// @Param id body string true "User ID"
// @Param username body string true "Username"
// @Param password body string true "Password"
// @Success 200 {object} map[string]string "message: User created successfully."
// @Failure 400 {object} map[string]string "message: Failed to create user."
// @Router /signup [post]
func SignupHandler(userManager *auth.UserManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Si la requête est une requête GET, affiche un formulaire HTML d'inscription.
		if r.Method == "GET" {
			fmt.Fprintln(w, `<html>
<head>
    <style>
        body { font-family: Arial, sans-serif; display: flex; justify-content: center; margin-top: 100px; }
        form { border: 1px solid #ccc; padding: 20px; box-shadow: 0 0 10px #ccc; }
        input[type=text], input[type=password] { width: 100%; padding: 10px; margin: 10px 0; }
        input[type=submit] { background-color: #4CAF50; color: white; padding: 10px 20px; border: none; cursor: pointer; }
        input[type=submit]:hover { background-color: #45a049; }
    </style>
</head>
<body>
    <form method="POST">
        ID: <input name="id" type="text" required><br>
        Username: <input name="username" type="text" required><br>
        Password: <input name="password" type="password" required><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>`)
			return
		}

		// Si la requête est une requête POST, cela signifie que le formulaire a été soumis.
		if r.Method == "POST" {
			// ParseForm lit les données du formulaire.
			if err := r.ParseForm(); err != nil {
				http.Error(w, "Failed to parse form", http.StatusBadRequest)
				return
			}

			// Récupère les valeurs du formulaire.
			id := r.FormValue("id")
			username := r.FormValue("username")
			password := r.FormValue("password")

			// Tente de créer un nouvel utilisateur avec ces informations.
			if err := userManager.CreateUser(id, username, password); err != nil {
				http.Error(w, "Failed to create user: "+err.Error(), http.StatusBadRequest)
				return
			}

			// Configure le 2FA pour le nouvel utilisateur et génère le QR Code.
			qrFileName, err := userManager.Setup2FA(id)
			if err != nil {
				http.Error(w, "Failed to setup 2FA: "+err.Error(), http.StatusInternalServerError)
				return
			}

			// Construit l'URL pour accéder au QR Code depuis le navigateur.
			qrURL := "/internal/qrcode/" + qrFileName

			// Inclut le QR Code directement dans la page HTML pour que l'utilisateur puisse le scanner.
			fmt.Fprintf(w, `<html>
<head>
    <style>
        body { font-family: Arial, sans-serif; text-align: center; margin-top: 50px; }
        h2 { color: #4CAF50; }
        img { margin-top: 20px; }
        a, .button-link { 
            display: inline-block; 
            margin-top: 20px; 
            background-color: #4CAF50; 
            color: white; 
            padding: 10px 20px; 
            text-decoration: none; 
            border-radius: 5px; // Ajoute un peu de style aux boutons
        }
        a:hover, .button-link:hover { background-color: #45a049; }
    </style>
</head>
<body>
    <h2>User created successfully!</h2>
    <p>Please scan the QR Code below to setup 2FA:</p>
    <img src="%s" alt="QR Code">
    <br>
    <a href="%s" download>Download QR Code</a>
    <br>
    <!-- Bouton de redirection vers la page de connexion -->
    <a href="/login" class="button-link">Go to Login</a>
</body>
</html>`, qrURL, qrURL)
			return
		}

		// Si la méthode HTTP n'est ni GET ni POST, renvoie une erreur.
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}
