package handlers

import (
	"fmt"
	"net/http"
	"tp-golang/internal/auth" // Utilisé pour authentifier les utilisateurs.
)

// LoginHandler gère la page et la logique de connexion des utilisateurs.
// @Summary Connexion utilisateur
// @Description Permet à un utilisateur de se connecter avec son nom d'utilisateur et mot de passe.
// @Tags Utilisateurs
// @Accept x-www-form-urlencoded
// @Produce html
// @Param username formData string true "Nom d'utilisateur"
// @Param password formData string true "Mot de passe"
// @Success 200 "Connexion réussie et redirection vers la vérification TOTP"
// @Failure 400 "Erreur de formulaire"
// @Failure 401 "Identifiants invalides"
// @Router /login [post]
func LoginHandler(userManager *auth.UserManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Affichage du formulaire de connexion pour les requêtes GET.
		if r.Method == "GET" {
			_, err := fmt.Fprintln(w, `<html>
<head>
    <style>
        body { font-family: Arial, sans-serif; display: flex; justify-content: center; margin-top: 100px; }
        form { border: 1px solid #ccc; padding: 20px; box-shadow: 0 0 10px #ccc; }
        input[type=text], input[type=password] { width: 100%; padding: 10px; margin: 10px 0; }
        input[type=submit] { background-color: #4CAF50; color: white; padding: 10px 20px; border: none; cursor: pointer; }
        input[type=submit]:hover { background-color: #45a049; }
    </style>
</head>
<body>
    <form method="POST">
        Username: <input name="username" type="text"><br>
        Password: <input name="password" type="password"><br>
        <input type="submit" value="Login">
    </form>
</body>
</html>`)
			if err != nil {
				return
			}
			return // Aucun besoin de vérifier l'erreur ici car le return suivant l'écriture dans le corps finalisera le handler.
		}

		// Vérification de la méthode de la requête pour les requêtes POST.
		if r.Method != "POST" {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		// Traitement de la soumission du formulaire pour les requêtes POST.
		if err := r.ParseForm(); err != nil {
			http.Error(w, "Failed to parse form", http.StatusBadRequest)
			return
		}

		// Vérification des identifiants de l'utilisateur.
		username := r.FormValue("username")
		password := r.FormValue("password")
		userID, authenticated := userManager.Authenticate(username, password)
		if !authenticated {
			// Si l'authentification échoue, envoie une erreur sans écrire d'autre message dans le corps de la réponse.
			http.Error(w, "Invalid username or password", http.StatusUnauthorized)
			return
		}

		// Si l'authentification est réussie, définit un cookie pour l'ID utilisateur et redirige vers la page de vérification TOTP.
		http.SetCookie(w, &http.Cookie{
			Name:   "user_id",
			Value:  userID,
			Path:   "/",
			MaxAge: 3600, //expire dans 1 heure
		})

		// La redirection doit être la dernière instruction exécutée dans le handler pour cette branche logique.
		http.Redirect(w, r, "/verifyTOTP", http.StatusSeeOther)
	}
}
