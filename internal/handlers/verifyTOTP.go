package handlers

import (
	"fmt"
	"github.com/pquerna/otp/totp"
	"net/http"
	"tp-golang/internal/auth"
)

// VerifyTOTPHandler Verify TOTP
// @Summary Verify TOTP code
// @Description Verify TOTP code entered by the user during the 2FA process.
// @Tags users
// @Accept json
// @Produce json
// @Param totpCode body string true "TOTP Code"
// @Success 200 {object} map[string]string "message: 2FA verification successful."
// @Failure 400 {object} map[string]string "message: TOTP code is required."
// @Failure 401 {object} map[string]string "message: Invalid TOTP code."
// @Router /verifyTOTP [post]
func VerifyTOTPHandler(userManager *auth.UserManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {
			// Affiche un formulaire où l'utilisateur peut entrer son code TOTP.
			_, err := fmt.Fprintln(w, `<html>
<head>
    <style>
        body { font-family: Arial, sans-serif; display: flex; justify-content: center; margin-top: 100px; }
        form { border: 1px solid #ccc; padding: 20px; box-shadow: 0 0 10px #ccc; }
        input[type=text], input[type=password] { width: 100%; padding: 10px; margin: 10px 0; }
        input[type=submit] { background-color: #4CAF50; color: white; padding: 10px 20px; border: none; cursor: pointer; }
        input[type=submit]:hover { background-color: #45a049; }
    </style>
</head>
<body>
    <form method="POST">
        Enter TOTP code: <input name="totpCode" type="text" required/><br>
        <input type="submit" value="Verify">
    </form>
</body>
</html>`)
			if err != nil {
				return
			}
			return
		}

		// Traitement du formulaire TOTP pour les requêtes POST.
		userIDCookie, err := r.Cookie("user_id")
		if err != nil {
			http.Error(w, "Session invalid", http.StatusBadRequest)
			return
		}
		userID := userIDCookie.Value

		totpCode := r.FormValue("totpCode")
		if totpCode == "" {
			http.Error(w, "TOTP code is required", http.StatusBadRequest)
			return
		}

		user, exists := userManager.GetUser(userID)
		if !exists {
			http.Error(w, "User not found", http.StatusNotFound)
			return
		}

		// Vérifie le code TOTP.
		if totp.Validate(totpCode, user.Secret2FA) {
			_, err := fmt.Fprintln(w, `<html><body><h2 style="color:green;">🎉 2FA verification successful. User is fully authenticated. 🎉</h2></body></html>`)
			if err != nil {
				return
			}
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			_, err := fmt.Fprintln(w, `<html><body><h2 style="color:red;">🚨 Invalid TOTP code 🚨</h2></body></html>`)
			if err != nil {
				return
			}
		}
	}
}
