package handlers

import (
	"fmt"
	"net/http"
	"tp-golang/internal/server" // Nécessaire pour vérifier les codes d'accès aux ressources protégées.
)

// ProtectedHandler vérifie l'accès aux ressources protégées en utilisant le code 2FA.
// @Summary Accéder à une ressource protégée
// @Description Vérifie si le code 2FA fourni par l'utilisateur est valide pour accéder à une ressource protégée.
// @Tags Ressources Protégées
// @Accept json
// @Produce json
// @Param code query string true "Code 2FA"
// @Success 200 "Accès accordé"
// @Failure 401 "Accès refusé"
// @Router /protected [get]
func ProtectedHandler(resourceServer *server.ResourceServer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Extraction et vérification du code 2FA fourni.
		code := r.URL.Query().Get("code")
		if resourceServer.ValidateCode(code) {
			// Accès accordé en cas de code valide.
			_, err := fmt.Fprintln(w, "Access Granted")
			if err != nil {
				return
			}
			return
		}
		// Accès refusé en cas de code invalide.
		http.Error(w, "Access Denied", http.StatusUnauthorized)
	}
}
