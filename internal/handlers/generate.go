package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"
	"time"
	"tp-golang/internal/auth"
)

// Response2FA contient le code 2FA généré et son expiration.
type Response2FA struct {
	Code       string `json:"code"`
	Expiration string `json:"expiration"`
}

// GenerateHandler gère la génération de codes 2FA en utilisant des goroutines pour améliorer la performance.
func GenerateHandler(authServer *auth.AuthServer, userManager *auth.UserManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userIDs := r.URL.Query()["userID"] // Supposons que nous puissions recevoir plusieurs userID
		if len(userIDs) == 0 {
			http.Error(w, "At least one userID is required", http.StatusBadRequest)
			return
		}

		var wg sync.WaitGroup
		responses := make([]Response2FA, len(userIDs))
		errors := make([]error, len(userIDs))

		for i, userID := range userIDs {
			if !userManager.Exists(userID) {
				errors[i] = http.ErrMissingFile
				continue
			}

			wg.Add(1)
			go func(index int, uid string) {
				defer wg.Done()
				code := authServer.GenerateCode(uid)
				expiration := authServer.GetCodeExpiration(code)
				responses[index] = Response2FA{
					Code:       code,
					Expiration: expiration.Format(time.RFC3339),
				}
			}(i, userID)

		}

		wg.Wait()

		// Vérifier s'il y a des erreurs
		for _, err := range errors {
			if err != nil {
				http.Error(w, "Failed to generate some codes: "+err.Error(), http.StatusInternalServerError)
				return
			}
		}

		w.Header().Set("Content-Type", "application/json")
		if err := json.NewEncoder(w).Encode(responses); err != nil {
			log.Printf("Error encoding response: %v", err)
			http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		}
	}
}
