package main

import (
	"github.com/go-redis/redis/v8"
	"log"
	"net/http"
	"os"
	"tp-golang/internal/auth"
	"tp-golang/internal/handlers"
	"tp-golang/internal/server"
)

// CorsMiddleware ajoute les en-têtes CORS nécessaires aux réponses.
func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func main() {
	redisHost := os.Getenv("REDIS_HOST")
	redisPort := os.Getenv("REDIS_PORT")
	rdb := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Password: "",
		DB:       0,
	})

	userManager := auth.NewUserManager()
	authServer := auth.NewAuthServer(rdb, userManager)
	resourceServer := server.NewResourceServer(authServer)

	mux := http.NewServeMux()
	mux.HandleFunc("/signup", handlers.SignupHandler(userManager))
	mux.HandleFunc("/login", handlers.LoginHandler(userManager))
	mux.HandleFunc("/generate", handlers.GenerateHandler(authServer, userManager))
	mux.HandleFunc("/protected", handlers.ProtectedHandler(resourceServer))
	mux.HandleFunc("/verifyTOTP", handlers.VerifyTOTPHandler(userManager))
	mux.Handle("/swaggerui/", http.StripPrefix("/swaggerui/", http.FileServer(http.Dir("swaggerui"))))
	mux.Handle("/swagger/", http.StripPrefix("/swagger/", http.FileServer(http.Dir("docs"))))
	mux.Handle("/internal/qrcode/", http.StripPrefix("/internal/qrcode/", http.FileServer(http.Dir("internal/qrcode"))))

	http.Handle("/", CorsMiddleware(mux))

	log.Println("Démarrage du serveur HTTPS sur :9443")
	log.Fatal(http.ListenAndServeTLS(":9443", "server.crt", "server.key", nil))
}
