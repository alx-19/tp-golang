// Package docs Code generated by swaggo/swag. DO NOT EDIT
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/generate": {
            "get": {
                "description": "Génère un code 2FA et son expiration pour un utilisateur donné.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "2FA"
                ],
                "summary": "Générer un code 2FA",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Identifiant de l'utilisateur",
                        "name": "userID",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Code 2FA généré avec succès",
                        "schema": {
                            "$ref": "#/definitions/handlers.Response2FA"
                        }
                    },
                    "400": {
                        "description": "Identifiant utilisateur invalide ou manquant",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/login": {
            "post": {
                "description": "Permet à un utilisateur de se connecter avec son nom d'utilisateur et mot de passe.",
                "consumes": [
                    "application/x-www-form-urlencoded"
                ],
                "produces": [
                    "text/html"
                ],
                "tags": [
                    "Utilisateurs"
                ],
                "summary": "Connexion utilisateur",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Nom d'utilisateur",
                        "name": "username",
                        "in": "formData",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "Mot de passe",
                        "name": "password",
                        "in": "formData",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Connexion réussie et redirection vers la vérification TOTP"
                    },
                    "400": {
                        "description": "Erreur de formulaire"
                    },
                    "401": {
                        "description": "Identifiants invalides"
                    }
                }
            }
        },
        "/protected": {
            "get": {
                "description": "Vérifie si le code 2FA fourni par l'utilisateur est valide pour accéder à une ressource protégée.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Ressources Protégées"
                ],
                "summary": "Accéder à une ressource protégée",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Code 2FA",
                        "name": "code",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Accès accordé"
                    },
                    "401": {
                        "description": "Accès refusé"
                    }
                }
            }
        },
        "/signup": {
            "post": {
                "description": "Signup a new user by providing ID, username, and password. User will then need to setup 2FA with the provided QR code.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "users"
                ],
                "summary": "Signup a new user",
                "parameters": [
                    {
                        "description": "User ID",
                        "name": "id",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "description": "Username",
                        "name": "username",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "description": "Password",
                        "name": "password",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "message: User created successfully.",
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    },
                    "400": {
                        "description": "message: Failed to create user.",
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        "/verifyTOTP": {
            "post": {
                "description": "Verify TOTP code entered by the user during the 2FA process.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "users"
                ],
                "summary": "Verify TOTP code",
                "parameters": [
                    {
                        "description": "TOTP Code",
                        "name": "totpCode",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "message: 2FA verification successful.",
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    },
                    "400": {
                        "description": "message: TOTP code is required.",
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    },
                    "401": {
                        "description": "message: Invalid TOTP code.",
                        "schema": {
                            "type": "object",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "handlers.Response2FA": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "string"
                },
                "expiration": {
                    "type": "string"
                }
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "",
	Host:             "",
	BasePath:         "",
	Schemes:          []string{},
	Title:            "",
	Description:      "",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
